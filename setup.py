# coding: utf-8
from setuptools import setup, find_packages  # noqa: H301

import setuptools
NAME = "yamltaday" #包名
VERSION = "1.0.0" # 版本
REQUIRES = ['pyyaml','datetime']  #依赖包

setup(
    name=NAME,
    version=VERSION,
    description="info",
    author="yjc",
    author_email="yjc@openapitools.org",
    url="",
    keywords=["yamltoday"], # 程序所在目录
    install_requires=REQUIRES,
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ], # 版本
)