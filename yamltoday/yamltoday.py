
from datetime import datetime

# 获取当前日期
import os
import yaml
'''
path 为绝对路径则在绝对路径创建
如果为相对路径则在当前工作目录下面创建
'''
class yaml_today:
    '''
    write_data_to_yaml_today: 读取 
        file_path：文件路径
        file_name：文件名
        
    read_data_to_yaml_today:  写入
        file_path：文件路径
        file_name：文件名
        
    '''
    @classmethod
    def get_data_filename(cls):
        # 获取当天日期字符串
        date = datetime.now().date()
        formatted_now = date.strftime("%Y%m%d")
        return formatted_now
    @classmethod
    def yaml_today_path(cls,file_path,file_name=''):
        # 传入文件夹和yaml文件名生成路径名称
        filedate=cls.get_data_filename()
        if file_path[-1] != '/' or file_path[-1] != '\\':
            file_path+='\\'
        if file_name != '':
            if file_name[0] == '/' or file_name[0] == '\\':
                file_name=file_name[1:]
        if 'yaml' in file_path or 'yaml' in file_name:
            yaml_path=file_path+filedate+'/'+file_name+'.yaml'
        else:
            yaml_path=file_path+filedate+'\\'+file_name+'.yaml'
        return yaml_path
        pass
    
    @classmethod
    def yaml_today_path_create(cls,file_path,file_name=''):
        # 传入文件夹和yaml文件名生创建文件路径
        yaml_today_path=cls.yaml_today_path(file_path,file_name)
        cr_yaml_today_path=os.path.split(yaml_today_path)[0]
        # print(yaml_today_path)
        
        if os.path.isabs(cr_yaml_today_path):
            cr_yaml_today_path=cr_yaml_today_path
        else:
            current_directory = os.getcwd()
            cr_yaml_today_path = os.path.join(current_directory, cr_yaml_today_path)
        print(yaml_today_path)
        if os.path.exists(cr_yaml_today_path):
            pass
        else:
            os.makedirs(cr_yaml_today_path)
        
        return yaml_today_path
    
    @classmethod
    def write_data_to_yaml_today(cls,data,file_path,file_name=''):
        # 写入到路径下今天的目录下
        '''
        写入文件
        data:要写入的数据
        file_path：文件路径
        file_name：文件名
        
        '''
        yaml_today_path=cls.yaml_today_path_create(file_path,file_name)  
        with open(yaml_today_path, 'w', encoding='utf-8') as file:
             yaml.safe_dump(data, file)
            
    
    @classmethod
    def read_data_to_yaml_today(cls,file_path,file_name=''):
        '''
        读取文件
        file_path：文件路径
        file_name：文件名
        
        '''
        # 读取
        yaml_today_path=cls.yaml_today_path_create(file_path,file_name)  
        with open(yaml_today_path, 'r', encoding='utf-8') as file:
             data=yaml.safe_load(file)
             return data
if __name__ == "__main__":
    print('test')
    yaml_today.get_data_filename()
    data={'a':1}
    a=yaml_today.write_data_to_yaml_today(data,file_path='log',file_name='memory')
    a=yaml_today.read_data_to_yaml_today(file_path='log',file_name='memory')
    print(a)